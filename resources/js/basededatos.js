conn = new Mongo();
db = conn.getDB("merkazone");

//USUARIOS
db.users.insert([
  {
    _id: ObjectId("5efbf39384c32b1e52474ca7"),
    name: "alejandra",
    email: "alejandra@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca0"),
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474ca8"),
    name: "mariam",
    email: "mariam@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca0"),
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474ca9"),
    name: "fabrizio",
    email: "fabrizio@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca1"),
  },
  {
    _id: ObjectId("5efc6ec784c32b1e52474caa"),
    name: "nikols",
    email: "nikols@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca1"),
  },
  {
    _id: ObjectId("5efc6ec784c32b1e52474cab"),
    name: "maria",
    email: "maria@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca2"),
  },
  {
    _id: ObjectId("5efc6ec784c32b1e52474cac"),
    name: "jhon",
    email: "jhon@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca3"),
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474cad"),
    name: "arturo",
    email: "arturo@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca3"),
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474cae"),
    name: "rosa",
    email: "rosa@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca3"),
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474caf"),
    name: "patrick",
    email: "patrick@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca4"),
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474cb0"),
    name: "diana",
    email: "diana@gmail.com",
    password: "$2y$10$VXsJR/ELLXHnHEdwf9//FuTrbpsZYRuK6024974XSeNDonNElkSPu", //contraseña
    rol: ObjectId("5efbf39384c32b1e52474ca4"),
  },
])

db.roles.insert([
  {
    _id: ObjectId("5efbf39384c32b1e52474ca0"),
    nombre: "Administrador del Sistema",
    valor: 1
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474ca1"),
    nombre: "Administrador de Tienda",
    valor: 2
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474ca2"),
    nombre: "Vendedor de Tienda",
    valor: 3
  },
  {
    _id: ObjectId("5efbf39384c32b1e52474ca3"),
    nombre: "Cliente",
    valor: 4
  },
])

//TRABAJDORES

