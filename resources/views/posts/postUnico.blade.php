<!--Extendemos de layouts.app-->
@extends('layouts.app')
<!--Implementamos el contenido de @ yeild('content')-->
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <h3 class="text-center">{{ $post->title }}</h3>
                <div class="card p-2 mb-2">
                    <img src="{{ asset($post->image) }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">{{ $post->content }}</p>
                        <a href="{{ action('PostController@index') }}" class="card-link float-right">Volver a todas las publicaciones</a>
                    </div>
                </div>
                <!--@ guest indica el contenido que se mostrara en la vista, para un usuario no autenticado-->
                @guest
                    <p>Si deseas comentar
                        <a href="{{ action('Auth\LoginController@showLoginForm') }}">Inicia Sesión</a>
                         o
                        <a href="{{ action('Auth\RegisterController@showRegistrationForm') }}">Registrate</a>
                    </p>
                @endguest
                <!--Seccion de comentarios-->
                <!--@ forelse es como un foreach solo si existe algun contendio en $post->comments()-->
                @if($post->comments->count()>0)
                <h4 class="mt-3">Comentarios</h4>
                @endif
                @forelse ($post->comments as $comment)
                <div class="card mb-2">
                    <div class="card-body">
                        <h5 class="card-title">{{ $comment->user->name }}</h5>
                        <hr>
                        <h6 class="card-subtitle mb-2 text-muted">{{ $comment->created_at }}</h6>
                        <p class="card-text">{{ $comment->content }}</p>
                    </div>
                </div>
                <!--@ empty si $post->comments() esta vacio-->
                @empty
                    <p class="mt-3">No hay comentarios en esta publicación, se el primero.</p>
                @endforelse
                <!--@ auth indica el contenido que se mostrara en la vista, para un usuario autenticado-->
                @auth
                <form class="form mt-3"  action="{{ action('CommentController@store') }}" method="POST">
                    @csrf  <!--@ csrf proteccion para ataques CSRF-->
                    <input name="post_id" value={{ $post->id }} hidden>
                    <div class="form-group">
                        <textarea name="content" class="form-control" rows="3" placeholder="Comenta"></textarea>
                    </div>
                    <div class="d-flex">
                        <button class="btn btn-danger mb-4 ml-auto"  type="submit">Submit</button>
                    </div>
                </form>
                @endauth
            </div>
        </div>
    </div>
@endsection
