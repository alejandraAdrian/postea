<!--Extendemos de layouts.app-->
@extends('layouts.app')
<!--Implementamos el contenido de @ yeild('content')-->
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <h2>Nueva publicación</h2>
        </div>
        <div class="row justify-content-md-center">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <p>¡Opss! Hubo problemas con los datos proporcionados</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ action('PostController@store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title" class="col-sm-2 col-form-label">{{ __('Title') }}</label>
                    <div class="col-sm-12">
                        <input id="title" class="form-control{{$errors->has('title') ? ' is-invalid' : ''}}"
                         type="text" name="title" value="{{ old('title') }}" autofocus>
                        @if ($errors->has('title'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="content" class="col-sm-2 col-form-label">{{ __('Content') }}</label>
                    <div class="col-sm-12">
                        <textarea rows="3" id="content" class="form-control{{$errors->has('content') ? ' is-invalid' : ''}}" name="content"> {{ old('content') }} </textarea>
                        @if ($errors->has('content'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('content') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="custom-file">
                            <input id="image" class="custom-file-input{{$errors->has('image') ? ' is-invalid' : ''}}" type="file" name="image">
                            <label for="customFile" class="custom-file-label">{{ __('Choose image') }}</label>
                            @if ($errors->has('image'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Create') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
