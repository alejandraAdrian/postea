@extends('layouts.app')
@section('content')

<div class="container">
    @auth
    <div class="text-right">
        <a href="{{ action('PostController@create') }}"
        class="btn btn-success mb-4"><i class="fas fa-plus-circle"></i> Crear una publicación</a>
        @if (Route::current()->getName() == "myPosts")
        <a href="{{ action('PostController@index') }}"
        class="btn btn-primary mb-4">Ver todas las publicaciones</a>
        @else
        <a href="{{ action('PostController@userPosts') }}"
        class="btn btn-primary mb-4">Ver mis publicaciones</a>
        @endif
    </div>
    @endauth
    <div class="row mb-4 justify-content-md-end">
        <div class="col-md-6">
            {{ $posts->links() }}
        </div>
    </div>
    @foreach ($posts as $post)
    <div class="row mb-4 justify-content-md-center">
        <div class="col-md-6">
            <div class="card" style="padding: 10px">
                <div class="card-boby">
                    <h5 class="card-title">
                        <a href="{{action('PostController@show', $post->id)}}">{{ $post->title }}</a>
                        @if (Route::current()->getName() == "myPosts")
                            <a href="{{action('PostController@destroy', $post->id)}}" class="btn btn-danger float-right mb-2 text-white">
                                <i class="fas fa-trash-alt"></i>
                                Eliminar
                            </a>
                            <a href="{{action('PostController@edit', $post->id)}}" class="btn btn-info float-right mb-2 mr-2 text-white">
                                <i class="fas fa-edit"></i>
                                Editar
                            </a>
                        @endif
                    </h5>
                </div>
                <!--
                    <img src="{{ Storage::url($post->image) }}" class="card-img-top" alt="...">
                -->
                <img src="{{ asset($post->image) }}" class="card-img-top" alt="...">
            </div>
        </div>
    </div>
    @endforeach
    <div class="row mb-4 justify-content-md-end">
        <div class="col-md-6">
            {{ $posts->links() }}
        </div>
    </div>
</div>
@endsection
