@component('mail::message')
# Bienvenido {{ $user->name }}
Ahora eres parte de {{ config('app.name') }}.

Gracias,<br>
{{ config('app.name') }}
@endcomponent
