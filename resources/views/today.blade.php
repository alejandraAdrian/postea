@extends('layouts.app')
@section('content')
<div class="container">
    <h2 class="text-center">POSTS DE HOY</h2>
    @foreach ($posts as $post)
    <div class="row mb-4 justify-content-md-center">
        <div class="col-md-6">
            <div class="card" style="padding: 10px">
                <div class="card-boby">
                    <h5 class="card-title">
                        <a href="{{action('PostController@show', $post->id)}}">{{ $post->title }}</a>
                    </h5>
                </div>
                <img src="{{ asset($post->image) }}" class="card-img-top" alt="...">
                <p class="text-right">{{$post->created_at}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection