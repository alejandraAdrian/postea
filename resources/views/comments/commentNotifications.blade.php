@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row mb-4">
        <div class="col-md-10 offset-1">
            @if (Auth::user()->notifications->count())
            <h5>Mis notificaciones</h5>
                @if (Auth::user()->unreadNotifications->count())
                <a href="{{ action('NotifyCommentController@markAsReadNotifyAll') }}"
                     class="btn btn-danger mb-2 text-white">
                    <i class="fas fa-share-square"></i>
                    Marcar todos como leidos
                </a>
                @endif
            @else
            <h5>Aun no tienes notificaciones</h5>
            @endif
        </div>
    </div>
    @forelse (Auth::user()->notifications as $notification)
    <div class="row mb-4 justify-content-md-center">
        <div class="col-md-10">
            <div class="card" style="padding: 10px">
                <div class="card-boby">
                @if($notification->unread())
                    <b>(No leido)</b>
                @else
                    <b>(Leido)</b>
                @endif
                    <p>{{ $notification->data["content"] }}</p>
                    <a href="{{action('NotifyCommentController@markAsReadNotify', $notification) }}"
                        class="btn btn-info float-right mb-2 text-white">
                        <i class="fas fa-share-square"></i>
                        Ir a la publicación
                    </a>
                </div>
            </div>
        </div>
    </div>
    @empty
    @endforelse
</div>
@endsection
