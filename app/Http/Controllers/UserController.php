<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(){
        $user_id = Auth::user()->id;
        $user = User::find($user_id,['name']);
        return view('user.edit',compact('user'));
    }

    public function update_name(Request $request)
    {
        $request->validate([
            'name' => ['required','string', 'max:255'],
            'name_password' => ['required','string', 'min:8'],
        ]);
        $user_id = Auth::user()->id;
        $user = User::find($user_id,['password']);
        if(Hash::check($request->get('name_password'),$user->password)){
            $user->name = $request->get('name');
            $user->update();
            return redirect()->route('posts');
        }
        return redirect()->route('edit_user');
    }

    public function update_pass(Request $request)
    {
        $request->validate([
            'past_password' => ['required','string', 'min:8'],
            'password' => ['required','string', 'min:8', 'confirmed'],
        ]);
        $user_id = Auth::user()->id;
        $user = User::find($user_id,['password']);
        if(Hash::check($request->get('past_password'),$user->password)){
            $password = Hash::make($request->get('password'));
            $user->password = $password;
            $user->update();
            return redirect()->route('posts');
        }
        return redirect()->route('edit_user');
    }

    public function destroy(Request $request){
        $request->validate([
            'delete_password' => ['required','string', 'min:8'],
        ]);
        $user_id = Auth::user()->id;
        $user = User::find($user_id,['password']);
        if(Hash::check($request->get('delete_password'),$user->password)){
            $user->posts()->delete();
            $user->delete();
            return redirect()->route('posts');
        }
        return redirect()->route('edit_user');
    }
}
