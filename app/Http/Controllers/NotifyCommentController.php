<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Foundation\Models\MongoDatabaseNotification;
use Illuminate\Notifications\Notification;
use DB;
use Illuminate\Http\Request;

class NotifyCommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('comments.commentNotifications');
    }

    public function markAsReadNotify($notification_id)
    {
        $notification = Auth::user()->notifications->find($notification_id);
        if ($notification->unread()){
            $notification->markAsRead();
        }
        return redirect()->route('post',['id'=>$notification->data["post_id"]]);
    }

    public function markAsReadNotifyAll()
    {
        Auth::user()->unreadNotifications->markAsRead();
        return redirect()->back();
    }
}

//$notificationsSamePost =  DB::table('notifications')->where('data["post_id"]',$notification->data["post_id"]);
//dd($notificationsSamePost);
