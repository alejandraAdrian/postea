<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index(){
        //$posts = Post::paginate(10);
        $posts = Post::orderBy('created_at','DESC')->paginate(10);
        return view('posts.index',compact('posts'));
    }

    public function show($id){
        return view('posts.postUnico',['post'=>Post::find($id)]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:120',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required|max:2200',
        ]);

        $imageName = $request->file('image')->store('posts/'.Auth::id(),'public');
        $title = $request->get('title');
        $content = $request->get('content');

        $post = $request->user()->posts()->create([
            'title' => $title,
            'image' => 'storage/'.$imageName,
            'content' => $content,
        ]);

        //$request->image->move(public_path('img'),$imageName);
        return redirect()->route('post', ['id' => $post->id]);
    }

    public function edit($id)
    {
        return view('posts.edit',['post'=>Post::find($id)]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:120',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required|max:2200',
        ]);

        $post = Post::find($id);
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $image = $request->file('image');
        //dd($image != null);
        if( $image != null){
            $imageName = time().$image->getClientOriginalName();
            $post->image = 'img/'.$imageName;
            $request->image->move(public_path('img'),$imageName);
        }
        $post->save();

        return redirect()->route('myPosts');
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect()->route('myPosts');
    }

    public function userPosts(){
        $user_id = Auth::id();
        $posts = Post::where('user_id',$user_id)->orderBy('created_at','DESC')->get();
        return view('posts.index',compact('posts'));
    }

    public function today(){
        $today = Carbon::now('America/Lima');
        //obtiene solo la fecha actual desde las 00:00horas con el mismo formato de ISODate
        $today = Carbon::now()->startOfDay();
        //compara que la fecha created_at sea mayor o igual a las 00:00 horas de hoy
        $posts = Post::where('created_at','>=',$today)->get();
        return view('today',compact('posts'));
    }
}
