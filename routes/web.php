<?php
Route::get('/email',function(){
    Mail::to("ale@tecs.com")->send(new WelcomeEmail());
    return new WelcomeEmail();
});


Route::get('/numerosletras',function(){
    return "Holi";
});
Route::redirect('/', '/posts');
Route::redirect('/home', '/posts');
//Route::view('/email', 'emails.welcome');

Route::get('/posts', 'PostController@index')->name('posts');
Route::get('/posts/create', 'PostController@create');
Route::post('/posts', 'PostController@store');
Route::get('/posts/myPosts', 'PostController@userPosts')->name('myPosts'); //para today
Route::get('/posts/{id}', 'PostController@show')->name('post');
Route::get('/posts/edit/{id}', 'PostController@edit');
Route::post('/posts/update/{id}', 'PostController@update');
Route::get('/posts/destroy/{id}', 'PostController@destroy');
//Route::post('/comments', 'CommentController@store');

Route::get('/user', 'UserController@edit')->name('edit_user');
Route::post('/user/destroy', 'UserController@destroy')->name('destroy_user');
Route::post('/user/update_name', 'UserController@update_name')->name('update_name');
Route::post('/user/update_pass', 'UserController@update_pass')->name('update_pass');
Route::get('/posts/today', 'PostController@today'); //para today
Route::post('/comments', 'CommentController@store');
Route::get('/markAsReadNotification/{notification}', 'NotifyCommentController@markAsReadNotify');
Route::get('/markAsReadNotifyAll', 'NotifyCommentController@markAsReadNotifyAll');
Route::get('/notifications', 'NotifyCommentController@index');

Auth::routes();

//Route::get('/home', 'PostController@index')->name('home');

