<?php

use Illuminate\Database\Seeder;
use App\User;
class UserCollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
            'email' => 'admin@example.com',
            'name'=>'admin',
            'password'=>'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi' //password
        ));

        //Sembradora Previa
        factory(App\User::class,4)
            ->create()
            ->each( function ($user){
                $user->posts()->createMany(
                    factory(App\Post::class,4)->make()->toArray()
                );
            });
    }


    /**
    //CON FOR
    for ($i = 1; $i <= 10; $i++) {
        factory(App\User::class)
        ->create(['counter' => $i])
        ->each( function ($user){
            $user->posts()->createMany(
                factory(App\Post::class,10)->make()->toArray()
            );
        });
    }

    //CON STORAGE
    public function run()
    {
        $users = factory(App\User::class,5)
        ->create()
        ->each( function ($user){
            $user->posts()->createMany(
                factory(App\Post::class,10)->create(['id'=>$user->id])
            );
        });
    }

     */
}
